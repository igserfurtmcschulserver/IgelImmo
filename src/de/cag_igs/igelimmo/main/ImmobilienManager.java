/*
IgelImmo - IgelImmobilien
----

Copyright (c) 2015-2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
package de.cag_igs.igelimmo.main;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.managers.RemovalStrategy;
import net.minecraft.server.v1_10_R1.BlockPurpurSlab;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.evilmidget38.UUIDFetcher;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.RegionGroup;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.managers.storage.StorageException;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class ImmobilienManager {
    private IgelImmo main;
    private ImmobilienWorldBuilder worldbuilder;

    public ImmobilienManager(IgelImmo m) {
        this.main = m;
        worldbuilder = new ImmobilienWorldBuilder(main);
    }

    private WorldGuardPlugin getWorldGuardPlugin() {
        Plugin plugin = this.main.getServer().getPluginManager().getPlugin("WorldGuard");
        if (plugin == null || !(plugin instanceof WorldGuardPlugin))
            return null;
        else
            return (WorldGuardPlugin) plugin;
    }

    public boolean createRegion(int length, Player owner, Location centralPoint, String regionname) {
        String world = centralPoint.getWorld().getName();
        if (length % 2 != 0) {
            owner.sendMessage(main.getLanguageFile().getConfig().getString("message.system.wrongplotsize"));
            return false;
        }
        Location firstcorner, secondcorner;
        firstcorner = new Location(this.main.getServer().getWorld(world), centralPoint.getX() + (length / 2), centralPoint.getY() + (length / 2), centralPoint.getZ() + (length / 2));
        secondcorner = new Location(this.main.getServer().getWorld(world), centralPoint.getX() - (length / 2), centralPoint.getY() - (length / 2), centralPoint.getZ() - (length / 2));
        BlockVector bvfirst = new BlockVector(firstcorner.getX(), 0, firstcorner.getZ());
        BlockVector bvseconde = new BlockVector(secondcorner.getX(), 255, secondcorner.getZ());
        ProtectedRegion region = new ProtectedCuboidRegion(regionname, bvfirst, bvseconde);
        region.setFlag(DefaultFlag.PASSTHROUGH, StateFlag.State.ALLOW);
        region.setFlag(DefaultFlag.PASSTHROUGH.getRegionGroupFlag(), RegionGroup.MEMBERS);
        region.setFlag(DefaultFlag.CHEST_ACCESS, StateFlag.State.ALLOW);
        region.setFlag(DefaultFlag.CHEST_ACCESS.getRegionGroupFlag(), RegionGroup.MEMBERS);
        region.setFlag(DefaultFlag.USE, StateFlag.State.ALLOW);
        region.setFlag(DefaultFlag.USE.getRegionGroupFlag(), RegionGroup.MEMBERS);
        region.setFlag(DefaultFlag.GREET_MESSAGE, String.format(main.getLanguageFile().getConfig().getString("greetmessage"), owner.getDisplayName()));
        region.setFlag(DefaultFlag.FAREWELL_MESSAGE, String.format(main.getLanguageFile().getConfig().getString("farewellmessage"), owner.getDisplayName()));
        //region.setFlag(DefaultFlag.GREET_MESSAGE, "Region " + region.getId());
        RegionContainer container = getWorldGuardPlugin().getRegionContainer();
        RegionManager regions = container.get(firstcorner.getWorld());
        if (!regions.overlapsUnownedRegion(region, getWorldGuardPlugin().wrapPlayer(owner))) {
            DefaultDomain owners = region.getOwners();
            owners.addPlayer(owner.getUniqueId());
            regions.addRegion(region);
            try {
                regions.save();
            } catch (StorageException e) {
                e.printStackTrace();
                return false;
            }
            this.main.getDatabaseControl().addNewPlot(centralPoint, owner.getLocation(), length / 2, regionname, false, false, false, true);
            this.main.getDatabaseControl().incrementPlotCount(owner.getUniqueId());
            this.main.getDatabaseControl().setOwner(regionname, owner.getUniqueId());
            worldbuilder.generate(centralPoint, length - 2, (length / 2) - 1, centralPoint.getWorld());
        } else {
            owner.sendMessage(main.getLanguageFile().getConfig().getString("message.plot.create.overlaps"));
            return false;
        }
        return true;
    }

    public String getPlotname(Location loc) {
        ApplicableRegionSet regions = this.getWorldGuardPlugin().getRegionManager(loc.getWorld()).getApplicableRegions(loc);
        for (ProtectedRegion region : regions) {
            if (region.getId().contains("_p")) {
                return region.getId();
            }
        }
        return "";
    }

    public boolean removeMember(Player owner, String membername, String plotname) {
        UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(membername));
        Map<String, UUID> response = null;
        try {
            response = fetcher.call();
        } catch (Exception e) {
            this.main.getLogger().warning("Exception while running UUIDFetcher");
            e.printStackTrace();
        }
        if (response != null && (response.get(membername) != null)) {
            RegionContainer container = getWorldGuardPlugin().getRegionContainer();
            RegionManager regions = container.get(owner.getWorld());
            if (regions != null) {
                ProtectedRegion region = regions.getRegion(plotname);
                if (region != null) {
                    DefaultDomain members = region.getMembers();
                    members.removePlayer(response.get(membername));
                    this.main.getDatabaseControl().removeMember(plotname, response.get(membername));
                    try {
                        regions.save();
                    } catch (StorageException e) {
                        this.main.getLogger().warning("Could not save Region!");
                        e.printStackTrace();
                        return false;
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

        } else {
            owner.sendMessage(String.format(main.getLanguageFile().getConfig().getString("message.plot.change.memberdontexist"), membername));
            return false;
        }
    }

    public boolean addMember(Player owner, String membername, String plotname) {
        UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(membername));
        Map<String, UUID> response = null;
        try {
            response = fetcher.call();
        } catch (Exception e) {
            this.main.getLogger().warning("Exception while running UUIDFetcher");
            e.printStackTrace();
        }
        if (response != null && (response.get(membername) != null)) {
            RegionContainer container = getWorldGuardPlugin().getRegionContainer();
            RegionManager regions = container.get(owner.getWorld());
            if (regions != null) {
                ProtectedRegion region = regions.getRegion(plotname);
                if (region != null) {
                    DefaultDomain members = region.getMembers();
                    members.addPlayer(response.get(membername));
                    this.main.getDatabaseControl().setMember(plotname, response.get(membername));
                    try {
                        regions.save();
                    } catch (StorageException e) {
                        this.main.getLogger().warning("Could not save Region!");
                        e.printStackTrace();
                        return false;
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

        } else {
            owner.sendMessage(String.format(main.getLanguageFile().getConfig().getString("message.plot.change.memberdontexist"), membername));
            return false;
        }
    }

    public boolean setOwner(Player owner, String membername, String plotname) {
        UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(membername));
        Map<String, UUID> response = null;
        try {
            response = fetcher.call();
        } catch (Exception e) {
            this.main.getLogger().warning("Exception while running UUIDFetcher");
            e.printStackTrace();
        }
        if (response != null && (response.get(membername) != null)) {
            RegionContainer container = getWorldGuardPlugin().getRegionContainer();
            RegionManager regions = container.get(owner.getWorld());
            if (regions != null) {
                ProtectedRegion region = regions.getRegion(plotname);
                if (region != null) {
                    DefaultDomain owners = region.getOwners();
                    owners.removePlayer(owner.getUniqueId());
                    owners.addPlayer(response.get(membername));
                    this.main.getDatabaseControl().removeOwner(plotname, owner.getUniqueId());
                    this.main.getDatabaseControl().setOwner(plotname, response.get(membername));
                    try {
                        regions.save();
                    } catch (StorageException e) {
                        this.main.getLogger().warning("Could not save Region!");
                        e.printStackTrace();
                        return false;
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

        } else {
            owner.sendMessage(String.format(main.getLanguageFile().getConfig().getString("message.plot.change.memberdontexist"), membername));
            return false;
        }
    }

    public boolean redefineRegion(String plotname, Selection selection) {
        RegionContainer container = getWorldGuardPlugin().getRegionContainer();
        RegionManager regions = container.get(main.getServer().getWorld(main.getDatabaseControl().getWorld(plotname)));
        if (regions != null) {
            ProtectedRegion region = regions.getRegion(plotname);
            Map<Flag<?>, Object> oldFlags = region.getFlags();
            DefaultDomain owners = region.getOwners();
            DefaultDomain members = region.getMembers();
            ProtectedRegion newRegion = new ProtectedCuboidRegion(plotname, new BlockVector(selection.getPos1().getX(), 0, selection.getPos1().getZ()), new BlockVector(selection.getPos2().getX(), 255, selection.getPos2().getZ()));
            newRegion.setFlags(oldFlags);
            newRegion.setOwners(owners);
            newRegion.setMembers(members);
            if (!regions.overlapsUnownedRegion(region, getWorldGuardPlugin().wrapPlayer(main.getServer().getPlayer(main.getDatabaseControl().getOwner(plotname))))) {
                regions.removeRegion(plotname, RemovalStrategy.UNSET_PARENT_IN_CHILDREN);
                regions.addRegion(region);
                try {
                    regions.save();
                } catch (StorageException e) {
                    e.printStackTrace();
                    return false;
                }
                main.getDatabaseControl().updatePlot(plotname, selection);
                return true;
            }
        } else {
            return false;
        }
        return false;
    }
}
