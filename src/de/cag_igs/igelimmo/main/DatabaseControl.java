/*
IgelImmo - IgelImmobilien
----

Copyright (c) 2015-2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
package de.cag_igs.igelimmo.main;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.bitbucket.igelborstel.igelcore.database.MySql;
import org.bukkit.Location;

public class DatabaseControl {
    private MySql database;
    private IgelImmo main;

    DatabaseControl(IgelImmo m) {
        this.main = m;
        database = new MySql(main.getDescription().getName());
        if (database.connect()) {
            createTables();
        }
    }

    public void close() {
        if (database.isConnected()) {
            database.close();
        }
    }

    private void createTables() {
        this.database.doUpdate("CREATE TABLE IF NOT EXISTS `igelimmobilien_plot` (name VARCHAR(40) NOT NULL PRIMARY KEY, world VARCHAR(16), px0 INT, pz0 INT, px1 INT, pz1 INT, tpx INT, tpy INT, tpz INT, flagpassthrough BOOLEAN, flagchestaccess BOOLEAN, flaguse BOOLEAN, flagpvp BOOLEAN)");
        this.database.doUpdate("CREATE TABLE IF NOT EXISTS `igelimmobilien_player` (uuid VARCHAR(36) NOT NULL PRIMARY KEY, plotcount INT, maximumplotcount INT, active BOOLEAN)");
        this.database.doUpdate("CREATE TABLE IF NOT EXISTS `igelimmobilien_guest` (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, name VARCHAR(40), flagpassthrough BOOLEAN, flagchestaccess BOOLEAN, flaguse BOOLEAN, flagpvp BOOLEAN)");
        this.database.doUpdate("CREATE TABLE IF NOT EXISTS `igelimmobilien_group` (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, uuid VARCHAR(36), name VARCHAR(40), owner BOOLEAN, member BOOLEAN, guest BOOLEAN)");
    }

    public void addNewPlayer(UUID uuid) {
        this.database.doUpdate("INSERT INTO `igelimmobilien_player` (`uuid`, `plotcount`, `maximumplotcount`, `active`) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE uuid=uuid", uuid.toString(), Integer.toString(0), Integer.toString(main.getConfig().getInt("general.maximumplot")), Integer.toString(1));
    }

    public void addNewPlot(Location loc, Location tploc, int radius, String name, boolean flagPassthrough, boolean flagChestAccess, boolean flagUse, boolean flagPVP) {
        int passthrough = (flagPassthrough) ? 1 : 0;
        int chestAccess = (flagChestAccess) ? 1 : 0;
        int use = (flagUse) ? 1 : 0;
        int pvp = (flagPVP) ? 1 : 0;
        this.database.doUpdate("INSERT INTO `igelimmobilien_plot` (`name`, `world`, `px0`, `pz0`, `px1`, `pz1`, `tpx`, `tpy`, `tpz`, `flagPassthrough`, `flagChestAccess`, `flagUse`, `flagPVP`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                name, loc.getWorld().getName(), Float.toString(loc.getBlockX() - radius), Float.toString((loc.getBlockZ() - radius)), Float.toString(loc.getBlockX() + radius), Float.toString((loc.getBlockZ() + radius)),
                Integer.toString((int) tploc.getX()), Integer.toString((int) tploc.getY()), Integer.toString((int) tploc.getZ()), Integer.toString(passthrough), Integer.toString(chestAccess), Integer.toString(use), Integer.toString(pvp));
    }

    public void incrementPlotCount(UUID uuid) {
        this.database.doUpdate("UPDATE `igelimmobilien_player` SET `plotcount` = `plotcount` + 1 WHERE `uuid`= ?", uuid.toString());
    }

    public void decrementPlotCount(UUID uuid) {
        this.database.doUpdate("UPDATE `igelimmobilien_player` SET `plotcount` = `plotcount` - 1 WHERE `uuid`= ?", uuid.toString());
    }

    public int getPlotCount(UUID uuid) {
        ResultSet results = this.database.doQuery("SELECT `plotcount` FROM `igelimmobilien_player` WHERE `uuid`=?", uuid.toString());
        try {
            while (results.next()) {
                return results.getInt("plotcount");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getMaxPlotCount(UUID uuid) {
        ResultSet results = this.database.doQuery("SELECT `maximumplotcount` FROM `igelimmobilien_player` WHERE `uuid`=?", uuid.toString());
        try {
            while (results.next()) {
                return results.getInt("maximumplotcount");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return main.getConfig().getInt("general.maximumplot");
    }

    public void setOwner(String regionname, UUID uniqueId) {
        this.database.doUpdate("INSERT INTO `igelimmobilien_group` (`uuid`, `name`, `owner`, `member`, `guest`) VALUES (?, ?, ?, ?, ?)", uniqueId.toString(), regionname, Integer.toString(1), Integer.toString(0), Integer.toString(0));
    }

    public boolean isOwner(String regionname, UUID uuid) {
        ResultSet results = this.database.doQuery("SELECT `owner` FROM `igelimmobilien_group` WHERE `uuid`=? AND `name`=?", uuid.toString(), regionname);
        try {
            while (results.next()) {
                return results.getBoolean("owner");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void setMember(String regionname, UUID uniqueId) {
        this.database.doUpdate("INSERT INTO `igelimmobilien_group` (`uuid`, `name`, `owner`, `member`, `guest`) VALUES (?, ?, ?, ?, ?)", uniqueId.toString(), regionname, Integer.toString(0), Integer.toString(1), Integer.toString(0));
    }

    public void removeMember(String regionname, UUID uniqueId) {
        this.database.doUpdate("DELETE FROM `igelimmobilien_group` WHERE `uuid`=? AND `name`=?", uniqueId.toString(), regionname);
    }

    public boolean isMember(String regionname, UUID uuid) {
        ResultSet results = this.database.doQuery("SELECT `member` FROM `igelimmobilien_group` WHERE `uuid`=? AND `name`=?", uuid.toString(), regionname);
        try {
            while (results.next()) {
                return results.getBoolean("member");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Location getTpLocation(String regionname) {
        ResultSet results = this.database.doQuery("SELECT `world`, `tpx`, `tpz`, `tpy` FROM `igelimmobilien_plot` WHERE `name`=?", regionname);
        int x = 0, y = 0, z = 0;
        String worldname = "world";
        try {
            while (results.next()) {
                x = results.getInt("tpx");
                y = results.getInt("tpy");
                z = results.getInt("tpz");
                worldname = results.getString("world");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Location(this.main.getServer().getWorld(worldname), x, y, z);
    }

    public void setTpLocation(String regionname, Location tploc) {
        this.database.doUpdate("UPDATE `igelimmobilien_plot` SET `tpx`=?, `tpy`=?, `tpz`=? WHERE `name`=?", Integer.toString((int) tploc.getX()), Integer.toString((int) tploc.getY()), Integer.toString((int) tploc.getZ()), regionname);
    }

    public ArrayList<UserGroup> getNameByUUID(UUID uuid) {
        ArrayList<UserGroup> temp = new ArrayList<UserGroup>();
        ResultSet results = this.database.doQuery("SELECT * FROM `igelimmobilien_group` WHERE `uuid`=?", uuid.toString());
        try {
            while (results.next()) {
                temp.add(new UserGroup(UUID.fromString(results.getString("uuid")), results.getString("name"), results.getBoolean("owner"), results.getBoolean("member"), results.getBoolean("guest")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return temp;
    }

    public ArrayList<UserGroup> getUUIDByName(String name) {
        ArrayList<UserGroup> temp = new ArrayList<UserGroup>();
        ResultSet results = this.database.doQuery("SELECT * FROM `igelimmobilien_group` WHERE `name`=?", name);
        try {
            while (results.next()) {
                temp.add(new UserGroup(UUID.fromString(results.getString("uuid")), results.getString("name"), results.getBoolean("owner"), results.getBoolean("member"), results.getBoolean("guest")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return temp;
    }

    public void removeOwner(String plotname, UUID uniqueId) {
        // TODO Auto-generated method stub
        this.database.doUpdate("DELETE FROM `igelimmobilien_group` WHERE `uuid`=? AND `name`=? AND `owner`=?", uniqueId.toString(), plotname, Integer.toString(1));
    }

    public void incrementMaxPlotCount(UUID uuid) {
        this.database.doUpdate("UPDATE `igelimmobilien_player` SET `maximumplotcount` = `maximumplotcount` + 1 WHERE `uuid`=?", uuid.toString());
    }

    public boolean toggleActive(UUID uuid) {
        this.database.doUpdate("UPDATE `igelimmobilien_player` SET `active` = !`active` WHERE `uuid`= ?", uuid.toString());
        ResultSet results = this.database.doQuery("SELECT `active` FROM `igelimmobilien_player` WHERE `uuid`=?", uuid.toString());
        try {
            while (results.next()) {
                return results.getBoolean("active");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isActive(UUID uuid) {
        ResultSet results = this.database.doQuery("SELECT `active` FROM `igelimmobilien_player` WHERE `uuid`=?", uuid.toString());
        try {
            while (results.next()) {
                return results.getBoolean("active");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getWorld(String plotname) {
        ResultSet results = this.database.doQuery("SELECT `world` FROM `igelimmoblien_plot` WHERE `name`=?", plotname);
        try {
            while (results.next()) {
                return results.getString("world");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getOwner(String plotname) {
        ResultSet results = this.database.doQuery("SELECT `uuid` FROM `igelimmoblien_group` WHERE `name`=? AND `owner`=TRUE", plotname);
        try {
            while (results.next()) {
                return results.getString("uuid");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void updatePlot(String plotname, Selection selection) {
        this.database.doUpdate("UPDATE `igelimmobilien_plot` SET `px0`=?, `pz0`=?, `px1`=?, `pz1`=? WHERE `name`=?", Integer.toString((int) selection.getPos1().getX()), Integer.toString((int) selection.getPos1().getZ()), Integer.toString((int) selection.getPos2().getX()), Integer.toString((int) selection.getPos2().getZ()), plotname);
    }
}
