/*
IgelImmo - IgelImmobilien
----

Copyright (c) 2015-2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
package de.cag_igs.igelimmo.commands;


import com.evilmidget38.UUIDFetcher;
import org.bitbucket.igelborstel.igelcore.commandHandlers.PlayerCommand;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;


import de.cag_igs.igelimmo.main.IgelImmo;
import de.cag_igs.igelimmo.main.UserGroup;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

public class MemberControlCommands extends PlayerCommand {

    private IgelImmo plugin;
    private boolean confirmNeeded = false;
    private boolean setowner = false;
    private Player player = null;
    private String arg = null;

    public MemberControlCommands(IgelImmo p) {
        this.plugin = p;
    }

    @Override
    public boolean command(Player player, Command cmd, String[] args) {
        if (cmd.getName().equalsIgnoreCase("membercontrol")) {
            if (args.length <= 0) {
                return false;
            }
            if (args[0].equalsIgnoreCase("list")) {
                if (args.length == 1) {
                    if (player.hasPermission("igelimmobilien.membercontrol.list")) {
                        String plotname = this.plugin.getImmobilienManager().getPlotname(player.getLocation());
                        if (plotname != null && !plotname.isEmpty()) {
                            if (plugin.getDatabaseControl().isOwner(plotname, player.getUniqueId()) ||
                                    (plugin.getDatabaseControl().isMember(plotname, player.getUniqueId()) && player.hasPermission("igelimmobilien.membercontrol.list.noowner"))) {
                                for (UserGroup usergroup : plugin.getDatabaseControl().getUUIDByName(this.plugin.getImmobilienManager().getPlotname(player.getLocation()))) {
                                    if (usergroup.isOwner()) {
                                        player.sendMessage(ChatColor.RED + "Owner: " + ChatColor.RESET + plugin.getNameByUUID(usergroup.getUuid()));
                                    } else if (usergroup.isMember()) {
                                        player.sendMessage(ChatColor.GREEN + "Member: " + ChatColor.RESET + plugin.getNameByUUID(usergroup.getUuid()));
                                    } else {
                                        player.sendMessage("Guest: " + ChatColor.RESET + plugin.getNameByUUID(usergroup.getUuid()));
                                    }
                                }
                            } else {
                                player.sendMessage("Du bist nicht Besitzer des Grundstücks und darfst dir die Mitspieler nicht ansehen!");
                            }
                        } else {
                            player.sendMessage("Dort wo du stehst befindet sich kein Grundstück!");
                        }
                        return true;
                    } else {
                        player.sendMessage("Du hast nicht genügend Rechte dir die eingetragen Grundstücksspieler anzusehen!");
                        return true;
                    }
                } else if (args.length == 2) {
                    if (player.hasPermission("igelimmobilien.membercontrol.list.mod")) {
                        for (UserGroup usergroup : plugin.getDatabaseControl().getUUIDByName(args[1])) {
                            if (usergroup.isOwner()) {
                                player.sendMessage(ChatColor.RED + "Owner: " + ChatColor.RESET + plugin.getNameByUUID(usergroup.getUuid()));
                            } else if (usergroup.isMember()) {
                                player.sendMessage(ChatColor.GREEN + "Member: " + ChatColor.RESET + plugin.getNameByUUID(usergroup.getUuid()));
                            } else {
                                player.sendMessage("Guest: " + ChatColor.RESET + plugin.getNameByUUID(usergroup.getUuid()));
                            }
                        }
                    } else {
                        player.sendMessage("Du hast nicht genügend Rechte um dir die Grundstücksmitglieder anzeigen zu lassen.");
                    }
                    return true;
                } else {
                    return false;
                }
            } else if (args[0].equalsIgnoreCase("add") && args.length == 2) {
                if (player.hasPermission("igelimmobilien.membercontrol.add")) {
                    if (plugin.getDatabaseControl().isOwner(this.plugin.getImmobilienManager().getPlotname(player.getLocation()), player.getUniqueId()) || player.hasPermission("igelimmobilien.membercontrol.add.mod")) {
                        if (plugin.getImmobilienManager().addMember(player, args[1], this.plugin.getImmobilienManager().getPlotname(player.getLocation()))) {
                            player.sendMessage("Spieler erfolgreich zum Grundstück hinzugefügt!");
                        }
                    } else {
                        player.sendMessage("Nur Besitzer dürfen Spieler zu einem Grundstück hinzufügen!");
                    }
                    return true;
                } else {
                    player.sendMessage("Du bist nicht berechtigt Spieler zu dem Grundstück hinzuzufügen!");
                }
                return true;
            } else if (args[0].equalsIgnoreCase("remove") && args.length == 2) {
                if (player.hasPermission("igelimmobilien.membercontrol.remove")) {
                    if (plugin.getDatabaseControl().isOwner(this.plugin.getImmobilienManager().getPlotname(player.getLocation()), player.getUniqueId()) || player.hasPermission("igelimmobilien.membercontrol.remove.mod")) {
                        if (plugin.getImmobilienManager().removeMember(player, args[1], this.plugin.getImmobilienManager().getPlotname(player.getLocation()))) {
                            player.sendMessage("Spieler erfolgreich vom Grundstück entfernt!");
                        }
                    } else {
                        player.sendMessage("Nur Besitzer dürfen Spieler von einem Grundstück entfernen!");
                    }
                } else {
                    player.sendMessage("Du bist nicht berechtigt Spieler von dem Grundstück zu entfernen!");
                }
                return true;
            } else if (args[0].equalsIgnoreCase("setowner") && args.length == 2) {
                if (player.hasPermission("igelimmobilien.membercontrol.setowner")) {
                    if (plugin.getDatabaseControl().isOwner(this.plugin.getImmobilienManager().getPlotname(player.getLocation()), player.getUniqueId()) || player.hasPermission("igelimmobilien.membercontrol.setowner.mod")) {
                        if (confirmNeeded) {
                            player.sendMessage("Owner setzen im Moment nicht möglich. Probiere dies später nocheinmal.");
                        } else {
                            player.sendMessage(Color.RED + "Warnung! " + Color.ORANGE + "Wenn ein anderer der Owner wird verlierst bist du nur noch einfacher Member auf dem Grundstück!");
                            player.sendMessage("Gebe innerhalb von 60 Sekunden /membercontrol confirm ein um dies zu bestätigen!");
                            confirmNeeded = true;
                            setowner = true;
                            this.player = player;
                            this.arg = args[1];
                            this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                                @Override
                                public void run() {
                                    confirmNeeded = false;
                                    setowner = false;
                                }
                            }, 1200);
                        }

                    } else {
                        player.sendMessage("Nur Owner dürfen andere Spieler zu Ownern machen");
                    }
                    return true;
                } else {
                    player.sendMessage("Du hast nicht genügend Rechte um eine Spieler zum Owner eines Grundstücks zu ernennen");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("confirm")) {
                if (confirmNeeded) {
                    confirmNeeded = false;
                    if (setowner) {
                        setowner = false;
                        if (player.equals(this.player)) {
                            if (this.plugin.getImmobilienManager().addMember(this.player, this.player.getDisplayName(), this.plugin.getImmobilienManager().getPlotname(player.getLocation()))) {
                                if (this.plugin.getImmobilienManager().setOwner(this.player, this.arg, this.plugin.getImmobilienManager().getPlotname(player.getLocation()))) {
                                    player.sendMessage("Spieler zum Owner ernannt!");
                                }
                            } else {
                                player.sendMessage("Spieler hinzufügen nicht möglich!");
                            }
                        }
                    }
                }
                return true;
            } else if (args[0].equalsIgnoreCase("incmaxplot") && args.length == 2) {
                if (player.hasPermission("igelimmobilien.membercontrol.incmaxplot")) {
                    UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(args[1]));
                    Map<String, UUID> response = null;
                    try {
                        response = fetcher.call();
                    } catch (Exception e) {
                        this.plugin.getLogger().warning("Exception while running UUIDFetcher");
                        e.printStackTrace();
                        player.sendMessage(ChatColor.RED + "Interner Fehler! Wende dich an einen Administrator!");
                        return true;
                    }
                    if (response != null && (response.get(args[1]) != null)) {
                        this.plugin.getDatabaseControl().incrementMaxPlotCount(response.get(args[1]));
                        player.sendMessage("Der Spieler " + args[1] + " kann jetzt ein weiteres Grundstück anlegen!");
                        return true;
                    }
                }
            } else if (args[0].equalsIgnoreCase("getmaxplot") && args.length == 2) {
                if (player.hasPermission("igelimmobilien.membercontrol.getmaxplot")) {
                    UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(args[1]));
                    Map<String, UUID> response = null;
                    try {
                        response = fetcher.call();
                    } catch (Exception e) {
                        this.plugin.getLogger().warning("Exception while running UUIDFetcher");
                        e.printStackTrace();
                        player.sendMessage(ChatColor.RED + "Interner Fehler! Wende dich an einen Administrator!");
                        return true;
                    }
                    if (response != null && (response.get(args[1]) != null)) {
                        int maxplot = this.plugin.getDatabaseControl().getMaxPlotCount(response.get(args[1]));
                        player.sendMessage("Der Spieler " + args[1] + " kann " + maxplot + " Grundstücke erstellen!");
                        return true;
                    }
                }
            } else {
                return false;
            }
        }
        // TODO Auto-generated method stub
        return false;
    }

}
