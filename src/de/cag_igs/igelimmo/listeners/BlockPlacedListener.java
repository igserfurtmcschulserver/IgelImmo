/*
IgelImmo - IgelImmobilien
----

Copyright (c) 2015-2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
package de.cag_igs.igelimmo.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

import de.cag_igs.igelimmo.main.IgelImmo;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class BlockPlacedListener implements Listener {
    private IgelImmo plugin;
    private static HashMap<UUID, Integer> datamap;
    private static HashMap<UUID, Long> timemap; //speichert Zeiten
    private static HashMap<UUID, Integer> warningmap; //Hat der Spieler eine Warnung bekommen?
    private static HashMap<UUID, Long> neededTimeMap;
    private static HashMap<UUID, Long> neededCountMap;

    public BlockPlacedListener(IgelImmo p) {
        this.plugin = p;
        datamap = new HashMap<>();
        timemap = new HashMap<>();
        warningmap = new HashMap<>();
        neededTimeMap = new HashMap<>();
        neededCountMap = new HashMap<>();
    }


    @EventHandler
    public void onBlockPlaced(BlockPlaceEvent event) {
        //Ploterstellung
        if (event.getBlock().getType() == Material.getMaterial(plugin.getConfig().getString("material.create").toUpperCase()) && event.getBlock().getLocation().getWorld().getName().equalsIgnoreCase(plugin.getConfig().getString("general.world"))) {
            if (event.getPlayer().hasPermission("igelimmobilien.plot.create")) {
                if (plugin.getDatabaseControl().getPlotCount(event.getPlayer().getUniqueId()) < plugin.getDatabaseControl().getMaxPlotCount(event.getPlayer().getUniqueId()) || event.getPlayer().hasPermission("igelimmobilien.ignore.maxplot")) {
                    if (plugin.getDatabaseControl().isActive(event.getPlayer().getUniqueId())) {
                        if (plugin.getImmobilienManager().createRegion(plugin.getConfig().getInt("general.radius") * 2, event.getPlayer(), event.getBlock().getLocation(), generatePlotName(event.getPlayer(), event.getBlock().getLocation()))) {
                            event.getPlayer().sendMessage(String.format(plugin.getLanguageFile().getConfig().getString("message.plot.create.sucessfuly"), generatePlotName(event.getPlayer(), event.getBlock().getLocation())));
                        }
                    } else {
                        event.getPlayer().sendMessage(plugin.getLanguageFile().getConfig().getString("message.plot.create.disabled"));
                    }
                } else {
                    event.getPlayer().sendMessage(plugin.getLanguageFile().getConfig().getString("message.plot.create.toomanyplots"));
                    event.setCancelled(true);
                }
            } else {
                event.getPlayer().sendMessage(plugin.getLanguageFile().getConfig().getString("message.plot.create.notenoughrights"));
                event.setCancelled(true);
            }
        }
        //Warnung für Blockplatzierung außerhalb des Grundstückes.
        if (event.getBlock().getWorld().getName().equalsIgnoreCase(plugin.getConfig().getString("general.world"))) {
            String plotname = this.plugin.getImmobilienManager().getPlotname(event.getBlock().getLocation());
            if (plotname != null && plotname.isEmpty()) {
                if (plugin.getConfig().getBoolean("protectWorld")) {
                  if (!event.getPlayer().hasPermission("igelimmobilien.util.ignoreWorldProtection")) {
                      event.setCancelled(true);
                      event.getPlayer().sendMessage(plugin.getLanguageFile().getConfig().getString("message.protection.forbidden.blockplacing"));
                  }
                } else if(plugin.getConfig().getBoolean("outsidewarning.enabled")) {
                    if (!(plugin.getDatabaseControl().isOwner(plotname, event.getPlayer().getUniqueId()) || plugin.getDatabaseControl().isMember(plotname, event.getPlayer().getUniqueId()))) {
                        if (datamap.containsKey(event.getPlayer().getUniqueId())) {
                            int old = datamap.get(event.getPlayer().getUniqueId());
                            datamap.put(event.getPlayer().getUniqueId(), ++old);
                            long oldTime = TimeUnit.NANOSECONDS.toMillis(timemap.get(event.getPlayer().getUniqueId()));
                            long currentNanoTime = System.nanoTime();
                            long currentTime = TimeUnit.NANOSECONDS.toMillis(currentNanoTime);

                            if (old == plugin.getConfig().getLong("outsidewarning.firstwarning.count")) {
                                if (currentTime - oldTime >= plugin.getConfig().getLong("outsidewarning.firstwarning.time")) {
                                    timemap.put(event.getPlayer().getUniqueId(), currentNanoTime);
                                    datamap.put(event.getPlayer().getUniqueId(), 1);
                                    warningmap.put(event.getPlayer().getUniqueId(), 0);
                                } else {
                                    event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getLanguageFile().getConfig().getString("message.outsidewarning.firstwarning")));
                                    warningmap.put(event.getPlayer().getUniqueId(), 1);
                                    return;
                                }
                            } else if (old == plugin.getConfig().getLong("outsidewarning.secondwarning.count") && warningmap.get(event.getPlayer().getUniqueId()) == 1) {
                                if (currentTime - oldTime >= plugin.getConfig().getLong("outsidewarning.secondwarning.time")) {
                                    timemap.put(event.getPlayer().getUniqueId(), currentNanoTime);
                                    datamap.put(event.getPlayer().getUniqueId(), 1);
                                } else {
                                    event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getLanguageFile().getConfig().getString("message.outsidewarning.secondwarning")));
                                    warningmap.put(event.getPlayer().getUniqueId(), 2);
                                    return;
                                }
                            } else if (old == neededCountMap.get(event.getPlayer().getUniqueId()) && (warningmap.get(event.getPlayer().getUniqueId()) == 2 || warningmap.get(event.getPlayer().getUniqueId()) == 3)) {
                                if (currentTime - oldTime >= neededTimeMap.get(event.getPlayer().getUniqueId())) {
                                    timemap.put(event.getPlayer().getUniqueId(), currentNanoTime);
                                    datamap.put(event.getPlayer().getUniqueId(), 1);
                                } else {
                                    event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getLanguageFile().getConfig().getString("message.outsidewarning.finalwarning")));
                                    warningmap.put(event.getPlayer().getUniqueId(), 3);
                                    neededTimeMap.put(event.getPlayer().getUniqueId(), neededTimeMap.get(event.getPlayer().getUniqueId()) + plugin.getConfig().getLong("outsidewarning.finalwarning.time"));
                                    neededCountMap.put(event.getPlayer().getUniqueId(), neededCountMap.get(event.getPlayer().getUniqueId()) + plugin.getConfig().getLong("outsidewarning.finalwarning.divider"));
                                    return;
                                }
                            }

                            if (currentTime - oldTime >= plugin.getConfig().getLong("outsidewarning.firstwarning.expire") && warningmap.get(event.getPlayer().getUniqueId()) == 1) {
                                timemap.put(event.getPlayer().getUniqueId(), currentNanoTime);
                                datamap.put(event.getPlayer().getUniqueId(), 1);
                                warningmap.put(event.getPlayer().getUniqueId(), 0);
                            } else if (currentTime - oldTime >= plugin.getConfig().getLong("outsidewarning.secondwarning.expire") && warningmap.get(event.getPlayer().getUniqueId()) == 2) {
                                timemap.put(event.getPlayer().getUniqueId(), currentNanoTime);
                                datamap.put(event.getPlayer().getUniqueId(), 1);
                                warningmap.put(event.getPlayer().getUniqueId(), 0);
                            } else if (currentTime - oldTime >= plugin.getConfig().getLong("outsidewarning.finalwarning.expire") && warningmap.get(event.getPlayer().getUniqueId()) == 3) {
                                timemap.put(event.getPlayer().getUniqueId(), currentNanoTime);
                                datamap.put(event.getPlayer().getUniqueId(), 1);
                                warningmap.put(event.getPlayer().getUniqueId(), 0);
                            }
                        } else {
                            datamap.put(event.getPlayer().getUniqueId(), 1);
                            timemap.put(event.getPlayer().getUniqueId(), System.nanoTime());
                            warningmap.put(event.getPlayer().getUniqueId(), 0);
                            neededTimeMap.put(event.getPlayer().getUniqueId(), plugin.getConfig().getLong("outsidewarning.secondwarning.time") + plugin.getConfig().getLong("outsidewarning.finalwarning.time"));
                            neededCountMap.put(event.getPlayer().getUniqueId(), plugin.getConfig().getLong("outsidewarning.secondwarning.count") + plugin.getConfig().getLong("outsidewarning.finalwarning.divider"));
                        }
                    }
                }
            }
        }
    }

    private String generatePlotName(Player player, Location middlePoint) {
        String temp = player.getDisplayName() + '_' + middlePoint.getWorld().getName().toString() + '_' + (int) middlePoint.getX() + (int) middlePoint.getY() + "_p";
        temp = temp.toLowerCase();
        return temp;
    }
}	
